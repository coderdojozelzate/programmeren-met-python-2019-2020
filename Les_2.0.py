# Rekenen met Python
print(8 * 3.57)
print(10 * 365)
print(20 + 3650)
print(3 * 52)
print(3670 - 156)

# De volgorde van bewerkingen
# * en / hebben altijd voorrang op optellen en aftrekken,
# tenzij er haakjes gebruikt worden om de volgorde van de berekingen te bepalen
print(5 + 30 * 20)
print((5 + 30) * 20)
print(((5+30)*20)/10)
print(5+30*20/10)

# Variabelen lijken op labels
fred = 100
print(fred)

fred = 200
print(fred)

fred = 200
jan = fred
print(jan)

aantal_munten = 200
print(aantal_munten)

# variabelen gebruiken
gevonden_munten = 20
magische_munten = 10
gestolen_munten = 3
print(gevonden_munten + magische_munten * 365 - gestolen_munten * 52)

gestolen_munten = 2
print(gevonden_munten + magische_munten * 365 - gestolen_munten * 52)

magische_munten = 13
print(gevonden_munten + magische_munten * 365 - gestolen_munten * 52)