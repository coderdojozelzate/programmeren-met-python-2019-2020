# Strings, Lijsten, Tupels en Woordenboeken
fred = "Waarom hebben gorillas grote neusgaten? Omdat ze grote vingers hebben!"
print(fred)

fred = 'Wat wordt rozig en pluizig? Roze pluisjes!'
print(fred)

# SyntaxError: EOL while scanning string literal
# fred = "Hoe betalen dinosaurussen hun rekeningen?
# print(fred)

# Meerdere regels in een string
fred = '''Hoe betalen dinosaurussen hun rekeningen?
Met Tyrannosaurus cheques!'''
print(fred)

# Gebruik van ' en " in een string kan door ''' te gebruiken
# onzin_string = 'Hij zei, "Gorilla's paraplu's opa's oma's."'
# onzin_string = "Hij zei, "Gorilla's paraplu's opa's oma's.""
onzin_string = '''Hij zei, "Gorilla's paraplu's opa's oma's."'''
print(onzin_string)

# Gebruik van ' en " kan ook door het escape teken \ te gebruiken
onzin_string = 'Hij zei, "Gorilla\'s paraplu\'s opa\'s oma\'s."'
print(onzin_string)
onzin_string = "Hij zei, \"Gorilla's paraplu's opa's oma's.\""
print(onzin_string)