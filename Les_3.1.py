# Strings, Lijsten, Tupels en Woordenboeken

# Waarden in strings embedden
# Een v oorbeeld met variabele
mijnscore = 100
bericht = 'Mijn score is %s punten'
print(bericht % mijnscore)

# Hetzelfde voorbeeld zonder variabele
print(bericht % 1000)

# Verschillende waarden insluiten met gebruik van verschillende variabelen
grap_tekst = '%s: een lichaamsdeel om in het donker meubels te vinden'
lichaamsdeel1 = 'Knie'
lichaamsdeel2 = 'Scheenbeen'
print(grap_tekst % lichaamsdeel1)
print(grap_tekst % lichaamsdeel2)

# Meerdere plaatshouders in een string moeten tussen haakjes geplaatst worden en
# in de volgorde van gewenst gebruik
nums = 'Hier zie je het getal %s en het getal %s!'
print(nums % (0,8))

# 10 x 5 = 50, maar wat is 10 x a ?
print(10 * 'a')

# Dat kan gebruikt worden om bv. een string met een specifiek aantal spaties achter elkaar te zetten
# om weer te geven in de shell
spaties = ' ' * 25
print('%s Poepjesstraat 12' % spaties)
print('%s Glimkontjes' % spaties)
print()
print()
print('Geachte meneer')
print()
print('Ik wil u hierbij melden dat er buiten op het dak')
print('bij de wc dakpannen ontbreken.')
print('Volgens mij zijn ze laatst weggewaaid door winderigheid.')
print()
print('Groeten')
print('Jan Treuzel')

# Alsook om het scherm te vullen met irritante boodschappen
print(1000 * 'snert')
